SELECT IIF(LEN(Wslr)=5,Wslr,IIF(LEN(Wslr)=4,STUFF(Wslr,1,0,'0'),IIF(LEN(Wslr)=3,STUFF(Wslr,1,0,'00'),NULL))) AS Wslr
      ,IIF(LEN(Pdcn)=5,Pdcn,IIF(LEN(Pdcn)=4,STUFF(Pdcn,1,0,'0'),IIF(LEN(Pdcn)=3,STUFF(Pdcn,1,0,'00'),NULL))) AS Pdcn
      ,[MON_SPLIT_WEEK_PCT]
      ,[TUE_SPLIT_WEEK_PCT]
      ,[WED_SPLIT_WEEK_PCT]
      ,[THU_SPLIT_WEEK_PCT]
      ,[FRI_SPLIT_WEEK_PCT]
  INTO [AB].[dbo].[Fcst_Split_Update]
  FROM [AB].[dbo].[Fcst_Split]

SELECT [Yr] AS [Year]
      ,[Mon] AS [Month]
      ,[Brwy]
      ,IIF(LEN(Pdcn)=5,Pdcn,IIF(LEN(Pdcn)=4,STUFF(Pdcn,1,0,'0'),IIF(LEN(Pdcn)=3,STUFF(Pdcn,1,0,'00'),NULL))) AS Pdcn
      ,[Cycle]
  INTO [AB].[dbo].[Cycles_Update]
  FROM [AB].[dbo].[Cycles]
  ;

WITH input AS (
SELECT [AB].[dbo].[Fcst_14_15_16_BDL].[Date] 
      ,[AB].[dbo].[Fcst_14_15_16_BDL].WSLR
      ,[AB].[dbo].[Fcst_14_15_16_BDL].GPDCN
      ,[AB].[dbo].[Sourcing_15_16_WSC].Brwy
      ,[AB].[dbo].[WSC_Conversion].[Wsc_Dc]
      ,[AB].[dbo].[WSC_Conversion].WSLR_NUM
      ,[SalesQty]
      ,[AvgSalesQty]
      ,[Fcst1]
      ,[Fcst2]
      ,[Fcst3]
      ,[Fcst4]
      ,[Fcst5]
      ,[Fcst6]
      ,[AB].[dbo].[RMSE].RMSE
      ,[CASES_BBL]
      ,[Vol] = [SalesQty]/[CASES_BBL]
      ,[AB].[dbo].[TT1:TransitTime_Brwy-Wslr].[TransitTime] AS T1
      ,[AB].[dbo].[TT1:TransitTime_Brwy-Wslr].[TransitTime_SD] AS T1_sd
      ,[AB].[dbo].[TT2:TramsitTime_Brwy-Dc-Uniq].[TransitTime] AS T2
      ,[AB].[dbo].[TT2:TramsitTime_Brwy-Dc-Uniq].[TransitTime_SD] AS T2_sd
      ,[AB].[dbo].[TT3:TramsitTime_Dc-Wslr-Uniq].[TransitTime] AS T3
      ,[AB].[dbo].[TT3:TramsitTime_Dc-Wslr-Uniq].[TransitTime_SD] AS T3_sd  
      ,Convert(float,[MON_SPLIT_WEEK_PCT]) AS Mon_Split
      ,Convert(float,[TUE_SPLIT_WEEK_PCT]) AS Tue_Split
      ,Convert(float,[WED_SPLIT_WEEK_PCT]) AS Wed_Split
      ,Convert(float,[THU_SPLIT_WEEK_PCT]) AS Thu_Split
      ,Convert(float,[FRI_SPLIT_WEEK_PCT]) AS Fri_Split
      ,Convert(float,[Cycle]) AS Cycle
      ,[AC_Final]
  FROM [AB].[dbo].[Fcst_14_15_16_BDL]
   
INNER JOIN
       [AB].[dbo].[Sourcing_15_16_WSC]
    ON [AB].[dbo].[Sourcing_15_16_WSC].Wslr = [AB].[dbo].[Fcst_14_15_16_BDL].WSLR
   And [AB].[dbo].[Sourcing_15_16_WSC].Gpdcn = [AB].[dbo].[Fcst_14_15_16_BDL].GPDCN
   And [AB].[dbo].[Sourcing_15_16_WSC].Year = YEAR ([AB].[dbo].[Fcst_14_15_16_BDL].Date)

LEFT Join
       [AB].[dbo].[WSC_Conversion]
    ON [AB].[dbo].[WSC_Conversion].WSC_DC = [AB].[dbo].[Sourcing_15_16_WSC].Wsc_Dc

LEFT JOIN
       [AB].[dbo].[RMSE]
    ON [AB].[dbo].[RMSE].WSLR = [AB].[dbo].[Fcst_14_15_16_BDL].WSLR
   AND [AB].[dbo].[RMSE].GPDCN = [AB].[dbo].[Fcst_14_15_16_BDL].GPDCN
   AND [AB].[dbo].[RMSE].Date = [AB].[dbo].[Fcst_14_15_16_BDL].Date

LEFT JOIN
       [AB].[dbo].[TT1:TransitTime_Brwy-Wslr]
    ON [AB].[dbo].[TT1:TransitTime_Brwy-Wslr].Wslr = [AB].[dbo].[Fcst_14_15_16_BDL].WSLR
   AND [AB].[dbo].[TT1:TransitTime_Brwy-Wslr].Brwy = [AB].[dbo].[Sourcing_15_16_WSC].Brwy

LEFT JOIN
       [AB].[dbo].[TT2:TramsitTime_Brwy-Dc-Uniq]
    ON [AB].[dbo].[TT2:TramsitTime_Brwy-Dc-Uniq].[WSLR_NUM] = [AB].[dbo].[WSC_Conversion].WSLR_NUM
   AND [AB].[dbo].[TT2:TramsitTime_Brwy-Dc-Uniq].Brwy = [AB].[dbo].[Sourcing_15_16_WSC].Brwy
  
LEFT JOIN
       [AB].[dbo].[TT3:TramsitTime_Dc-Wslr-Uniq]
    ON [AB].[dbo].[TT3:TramsitTime_Dc-Wslr-Uniq].Wslr = [AB].[dbo].[Fcst_14_15_16_BDL].WSLR
   AND [AB].[dbo].[TT3:TramsitTime_Dc-Wslr-Uniq].[Wsc_Dc] = [AB].[dbo].[Sourcing_15_16_WSC].[Wsc_Dc]

LEFT JOIN
       [AB].[dbo].[Fcst_Split_Update]
    ON [AB].[dbo].[Fcst_Split_Update].[Wslr] = [AB].[dbo].[Fcst_14_15_16_BDL].WSLR
   AND [AB].[dbo].[Fcst_Split_Update].[Pdcn] = [AB].[dbo].[Fcst_14_15_16_BDL].GPDCN

INNER JOIN
       [AB].[dbo].[Cycles_Update]
    ON [AB].[dbo].[Cycles_Update].Pdcn = [AB].[dbo].[Fcst_14_15_16_BDL].GPDCN
   AND [AB].[dbo].[Cycles_Update].Brwy = [AB].[dbo].[Sourcing_15_16_WSC].Brwy
   AND [AB].[dbo].[Cycles_Update].Month = MONTH ([AB].[dbo].[Fcst_14_15_16_BDL].Date)
   And [AB].[dbo].[Cycles_Update].Year = YEAR ([AB].[dbo].[Fcst_14_15_16_BDL].Date)

LEFT JOIN
       [AB].[dbo].[AC_15_16_Update]
    ON [AB].[dbo].[AC_15_16_Update].Wslr = [AB].[dbo].[Fcst_14_15_16_BDL].WSLR
   AND [AB].[dbo].[AC_15_16_Update].Gpdcn = [AB].[dbo].[Fcst_14_15_16_BDL].GPDCN
   AND [AB].[dbo].[AC_15_16_Update].Year =  YEAR ([AB].[dbo].[Fcst_14_15_16_BDL].Date)

WHERE YEAR ([AB].[dbo].[Fcst_14_15_16_BDL].Date) > 2014)

SELECT [input].[Date]
      ,[input].[Wslr]
      ,[input].[Gpdcn]
      ,[input].[Brwy]
      ,[Wsc_Dc]
      ,[WSLR_NUM]
      ,[SalesQty]
      ,[AvgSalesQty]
      ,[Fcst1]
      ,[Fcst2]
      ,[Fcst3]
      ,[Fcst4]
      ,[Fcst5]
      ,[Fcst6]
      ,[RMSE]
      ,[CASES_BBL]
      ,[Vol]
      ,IIF(T1 IS NULL, 7, T1) AS T1
      ,IIF(T1_sd IS NULL, 2, T1_sd) AS T1_sd
      ,IIF(T2 IS NULL, 7, T2) AS T2
      ,IIF(T2_sd IS NULL, 2, T2_sd) AS T2_sd
      ,IIF(T3 IS NULL, 7, T3) AS T3
      ,IIF(T3_sd IS NULL, 2, T3_sd) AS T3_sd
      ,IIF([Mon_Split] IS NULL, 0.2,[Mon_Split]) AS Mon_Split
      ,IIF([Tue_Split] IS NULL, 0.2,[Tue_Split]) AS Tue_Split
      ,IIF([Wed_Split] IS NULL, 0.2,[Wed_Split]) AS Wed_Split
      ,IIF([Thu_Split] IS NULL, 0.2,[Thu_Split]) AS Thu_Split
      ,IIF([Fri_Split] IS NULL, 0.2,[Fri_Split]) AS Fri_Split
      ,[Cycle]
      ,[AC_Final]
      ,[Week_Number] = Row_Number() OVER (PARTITION BY WSLR, GPDCN ORDER BY [Date] DESC)
      ,[Mu_L] = IIF(AC_Final = 'A', T1 + (Cycle * 7.0), IIF( AC_Final = 'C', T2 + T3 + (Cycle * 7.0) + 7, 'NULL'))
      ,[Sigma_L] = IIF(AC_Final = 'A', SQRT(POWER(T1_sd,2) + 6.25), IIF( AC_Final = 'C', SQRT(POWER(T2_sd,2) + POWER(T3_sd,2) + 6.25 + 4) , 'NULL'))
      ,[Sigma_D] = SQRT(POWER(RMSE,2) / 7.0)
  INTO [AB].[dbo].[SS_Input]
  FROM [input]
 WHERE [Cycle] IS NOT NULL

ALTER TABLE [AB].[dbo].[SS_Input] ADD [Integer] float NULL 
GO
Update [AB].[dbo].[SS_Input] SET [Integer] = CAST(Mu_L as int) / 7

ALTER TABLE [AB].[dbo].[SS_Input] ADD [Remainder] float NULL 
GO
Update [AB].[dbo].[SS_Input] SET [Remainder] = [Mu_L] - ([Integer]*7)
