WITH TTState AS (
SELECT [Transit_Times_Cleansed_20160310].[Brwy]
      ,[Transit_Times_Cleansed_20160310].[Wslr]
      ,[WSLR_Data_Update].[State] AS WslrState
      ,CONVERT(float,TransitTime) AS TransitTime
      ,CONVERT (float,TransitTime_StDev) AS TransitTime_SD
      ,[Number_of_Records]
  FROM [AB].[dbo].[Transit_Times_Cleansed_20160310]

LEFT JOIN 
      [AB].[dbo].[WSLR_Data_Update]
   ON [AB].[dbo].[WSLR_Data_Update].Wslr = [Transit_Times_Cleansed_20160310].[Wslr])


SELECT [Brwy]
      ,[Wslr]
      ,[WslrState]
      ,[TransitTime]
      ,[TransitTime_SD]
      ,[Number_of_Records]
	  ,[AvgTransit_Brwy-State]= AVG(TransitTime) OVER (PARTITION BY Brwy, WslrState)
	  ,[AvgTransitSD_Brwy-State]= AVG(TransitTime_SD) OVER (PARTITION BY Brwy, WslrState)
  INTO [AB].[dbo].[TransitTimes_Brwy-State]  
  FROM TTState


SELECT [Sourcing_15_16_Wslr-Brwy_Uniq].[Brwy]
      ,[Sourcing_15_16_Wslr-Brwy_Uniq].[Wslr]
      ,[WslrState]
      ,IIF(TransitTime IS NULL, IIF(WslrState IS NULL, 7, [AvgTransit_Brwy-State]),TransitTime) AS TransitTime
      ,IIF(TransitTime_SD IS NULL, IIF(WslrState IS NULL, 2, [AvgTransitSD_Brwy-State]),TransitTime_SD) AS TransitTime_SD
      ,[AvgTransit_Brwy-State]
      ,[AvgTransitSD_Brwy-State]
  INTO [AB].[dbo].[TT1:TransitTime_Brwy-Wslr]
  FROM [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq]

LEFT JOIN
       [AB].[dbo].[TransitTimes_Brwy-State]
    ON [AB].[dbo].[TransitTimes_Brwy-State].Brwy = [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Brwy
   AND [AB].[dbo].[TransitTimes_Brwy-State].Wslr = [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Wslr


SELECT [AB].[dbo].[WSC_Conversion].WSLR_NUM
      ,[AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Brwy
      ,IIF([WSC_Conversion].[WSC_DC] = [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].[Brwy], 0
      ,IIF(TransitTime IS NULL, 7, TransitTime)) AS TransitTime
      ,IIF([WSC_Conversion].[WSC_DC] = [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].[Brwy], 0
      ,IIF(TransitTime_SD IS NULL, 2,TransitTime_SD)) AS TransitTime_SD
  INTO [AB].[dbo].[TT2:TramsitTime_Brwy-Dc]
  FROM [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq]
   
Left Join
       [AB].[dbo].[WSC_Conversion] ON
       [AB].[dbo].[WSC_Conversion].WSC_DC = [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Wsc_Dc

Left Join
       [AB].[dbo].[TT1:TransitTime_brwy-wslr] ON
       [AB].[dbo].[TT1:TransitTime_brwy-wslr].Wslr = [AB].[dbo].[WSC_Conversion].WSLR_NUM And
       [AB].[dbo].[TT1:TransitTime_brwy-wslr].Brwy = [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Brwy


SELECT [WSLR_NUM]
      ,[Brwy]
      ,[TransitTime]
      ,[TransitTime_SD]
      ,RN = Row_Number() Over (Partition by WSLR_NUM, Brwy Order by WSLR_NUM)
  INTO [AB].[dbo].[TT2:TramsitTime_Brwy-Dc-Uniq]
  FROM [AB].[dbo].[TT2:TramsitTime_Brwy-Dc]

  Delete from [AB].[dbo].[TT2:TramsitTime_Brwy-Dc-Uniq] where RN>1


SELECT [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Wsc_Dc
      ,[AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Wslr
      ,IIF(TransitTime IS NULL, 7, TransitTime) AS TransitTime
      ,IIF(TransitTime_SD IS NULL, 2,TransitTime_SD) AS TransitTime_SD
  INTO [AB].[dbo].[TT3:TramsitTime_DC-Wslr]
  FROM [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq]

Left Join
       [AB].[dbo].[TT1:TransitTime_brwy-wslr] ON
       [AB].[dbo].[TT1:TransitTime_brwy-wslr].Wslr = [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Wslr And
       [AB].[dbo].[TT1:TransitTime_brwy-wslr].Brwy = [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq].Wsc_Dc

SELECT [Wsc_Dc]
      ,[Wslr]
      ,[TransitTime]
      ,[TransitTime_SD]
	  ,RN = Row_Number() Over (Partition by Wsc_Dc, Wslr Order by Wslr)
  Into [AB].[dbo].[TT3:TramsitTime_Dc-Wslr-Uniq]
  FROM [AB].[dbo].[TT3:TramsitTime_DC-Wslr]

  Delete from [AB].[dbo].[TT3:TramsitTime_Dc-Wslr-Uniq] where RN>1
