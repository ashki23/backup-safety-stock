SELECT 
       IIF(LEN(["wslr"])=5,["wslr"],IIF(LEN(["wslr"])=4,STUFF(["wslr"],1,0,'0'),IIF(LEN(["wslr"])=3,STUFF(["wslr"],1,0,'00'),NULL))) AS WSLR
      ,IIF(LEN(["PDCN"])=5,["PDCN"],IIF(LEN(["PDCN"])=4,STUFF(["PDCN"],1,0,'0'),IIF(LEN(["PDCN"])=3,STUFF(["PDCN"],1,0,'00'),NULL))) AS GPDCN
      ,CONVERT(Date,STUFF(STUFF(["Week"],6,17,'-2016'),3,0,'-'),101) AS Date
      ,CONVERT(float,["SalesQty"]) AS SalesQty
      ,CONVERT(float,["AvgSalesQty"]) As AvgSalesQty
      ,CONVERT(float,["Fcst1"]) AS Fcst1
      ,CONVERT(float,["Fcst2"]) AS Fcst2
      ,CONVERT(float,["Fcst3"]) AS Fcst3
      ,CONVERT(float,["Fcst4"]) AS Fcst4
      ,CONVERT(float,["Fcst5"]) AS Fcst5
      ,CONVERT(float,["Fcst6"]) AS Fcst6
      ,CONVERT(float,["CASES_BBL"]) AS CASES_BBL
  INTO [AB].[dbo].[Fcst_14_15_16_BDL]
  FROM [AB].[dbo].[Fcst_Var_Data_2016]
  WHERE LEFT(["PDCN"],2)= '53' or  LEFT(["PDCN"],2)= '54' -- Selecting product ids starting with 53 or 54

UNION ALL

SELECT 
       IIF(LEN(["wslr"])=5,["wslr"],IIF(LEN(["wslr"])=4,STUFF(["wslr"],1,0,'0'),IIF(LEN(["wslr"])=3,STUFF(["wslr"],1,0,'00'),NULL))) AS WSLR
      ,IIF(LEN(["PDCN"])=5,["PDCN"],IIF(LEN(["PDCN"])=4,STUFF(["PDCN"],1,0,'0'),IIF(LEN(["PDCN"])=3,STUFF(["PDCN"],1,0,'00'),NULL))) AS GPDCN
      ,CONVERT(Date,STUFF(STUFF(["Week"],6,17,'-2015'),3,0,'-'),101) AS Date
      ,CONVERT(float,["SalesQty"]) AS SalesQty
      ,CONVERT(float,["AvgSalesQty"]) As AvgSalesQty
      ,CONVERT(float,["Fcst1"]) AS Fcst1
      ,CONVERT(float,["Fcst2"]) AS Fcst2
      ,CONVERT(float,["Fcst3"]) AS Fcst3
      ,CONVERT(float,["Fcst4"]) AS Fcst4
      ,CONVERT(float,["Fcst5"]) AS Fcst5
      ,CONVERT(float,["Fcst6"]) AS Fcst6
      ,CONVERT(float,["CASES_BBL"]) AS CASES_BBL
  FROM [AB].[dbo].[Fcst_Var_Data_2015]
  WHERE LEFT(["PDCN"],2)= '53' or  LEFT(["PDCN"],2)= '54' -- Selecting product ids starting with 53 or 54

UNION ALL

SELECT 
       IIF(LEN(["wslr"])=5,["wslr"],IIF(LEN(["wslr"])=4,STUFF(["wslr"],1,0,'0'),IIF(LEN(["wslr"])=3,STUFF(["wslr"],1,0,'00'),NULL))) AS WSLR
      ,IIF(LEN(["PDCN"])=5,["PDCN"],IIF(LEN(["PDCN"])=4,STUFF(["PDCN"],1,0,'0'),IIF(LEN(["PDCN"])=3,STUFF(["PDCN"],1,0,'00'),NULL))) AS GPDCN
      ,CONVERT(Date,STUFF(STUFF(["Week"],6,17,'-2014'),3,0,'-'),101) AS Date
      ,CONVERT(float,["SalesQty"]) AS SalesQty
      ,CONVERT(float,["AvgSalesQty"]) As AvgSalesQty
      ,CONVERT(float,["Fcst1"]) AS Fcst1
      ,CONVERT(float,["Fcst2"]) AS Fcst2
      ,CONVERT(float,["Fcst3"]) AS Fcst3
      ,CONVERT(float,["Fcst4"]) AS Fcst4
      ,CONVERT(float,["Fcst5"]) AS Fcst5
      ,CONVERT(float,["Fcst6"]) AS Fcst6
      ,CONVERT(float,LEFT(["CASES_BBL"],2)) AS CASES_BBL
  FROM [AB].[dbo].[Fcst_Var_Data_2014]
  WHERE LEFT(["PDCN"],2)= '53' or  LEFT(["PDCN"],2)= '54' -- Selecting product ids starting with 53 or 54
