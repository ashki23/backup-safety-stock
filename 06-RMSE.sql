WITH WSC_DC AS (
SELECT [Fcst_14_15_16_BDL].[WSLR]
      ,[GPDCN]
      ,[Date]
      ,[SalesQty]
      ,[AvgSalesQty]
      ,[Fcst1]
      ,[Fcst2]
      ,[Fcst3]
      ,[Fcst4]
      ,[Fcst5]
      ,[Fcst6]
      ,[CASES_BBL]
      ,[AB].[dbo].[WSLR_Data_Update].WSC_DC AS [WSC_DC] 
  FROM [AB].[dbo].[Fcst_14_15_16_BDL] 
  
LEFT JOIN
       [AB].[dbo].[WSLR_Data_Update] ON 
       [AB].[dbo].[WSLR_Data_Update].WSLR = [Fcst_14_15_16_BDL].WSLR)

SELECT *
       ,[SE] = POWER([Fcst3]-[SalesQty],2)
  INTO [AB].[dbo].[SE]
  FROM WSC_DC
  ORDER BY DATE

DELETE FROM [AB].[dbo].[SE] WHERE [WSC_DC] IS NULL


WITH rmse AS(
SELECT YEAR(Date) * 100 + DATEPART(WEEK, Date) AS Week
           ,Date
           ,RANK() OVER (PARTITION BY gpdcn, Wslr ORDER BY YEAR(Date) * 100 + DATEPART(WEEK, Date)) AS gpdcn_wslr_week_rank
           ,WSLR
           ,GPDCN
           ,SE
      FROM [AB].[dbo].[SE])

     SELECT CW.Date
           ,CW.WSLR
           ,CW.GPDCN
           ,CW.Week
           ,CW.gpdcn_wslr_week_rank
           ,CW.SE
           ,SQRT(AVG(BW.SE)) AS [RMSE]
      INTO [AB].[dbo].[RMSE]
      FROM rmse AS CW

INNER JOIN rmse BW
        ON BW.GPDCN = CW.GPDCN AND
           BW.WSLR = CW.WSLR AND
           CW.gpdcn_wslr_week_rank >  BW.gpdcn_wslr_week_rank AND
           BW.gpdcn_wslr_week_rank >= CW.gpdcn_wslr_week_rank - 52
 
  GROUP BY CW.Date
          ,CW.WSLR
          ,CW.GPDCN
          ,CW.Week
          ,CW.SE
          ,CW.gpdcn_wslr_week_rank 
 
  ORDER BY CW.WSLR
          ,CW.GPDCN
          ,CW.Week DESC