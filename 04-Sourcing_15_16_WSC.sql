With sourcing As (
Select *, [Year] = 2015 From [AB].[dbo].[Sourcing_2015] Union All
Select *, [Year] = 2016 From [AB].[dbo].[Sourcing_2016] )

Select * , [RN] = Row_Number () Over (Partition By Wslr, Gpdcn, Brwy Order By Wslr)
Into [AB].[dbo].[Sourcing_15_16_WSC]
From sourcing

Delete From [AB].[dbo].[Sourcing_15_16_WSC] Where LEN([Wsc_Dc]) < 1
Delete From [AB].[dbo].[Sourcing_15_16_WSC] Where RN > 1
;

With sourcing2 As (
Select *, [Year] = 2015 From [AB].[dbo].[Sourcing_2015] Union All
Select *, [Year] = 2016 From [AB].[dbo].[Sourcing_2016] )

Select * , [RN] = Row_Number () Over (Partition By Wslr, Brwy Order By Wslr)
Into [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq]
From sourcing2

Delete From [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq] Where LEN([Wsc_Dc]) < 1
Delete From [AB].[dbo].[Sourcing_15_16_Wslr-Brwy_Uniq] Where RN > 1
