WITH inpu AS (
SELECT [WSLR]
      ,[Z-value] 
  FROM [AB].[dbo].[AlterZ_Excess_WSLR_Results]

UNION ALL

SELECT [WSLR]
      ,[Z-value] 
  FROM [AB].[dbo].[AlterZ_Shortfall_WSLR_Results] )

SELECT [Date]
      ,[SafetyStock].[Wslr]
      ,[Gpdcn]
      ,[Brwy]
      ,[Wsc_Dc]
      ,[AC_Final]
      ,[Vol]
      ,[CASES_BBL]
      ,[T1], [T1_sd], [T2], [T2_sd], [T3], [T3_sd]
      ,[Cycle]
      ,[Actual_Demand]
      ,[Sigma] = CONVERT(DECIMAL(10,3), [SS]/2.85)
      ,[Mu] = [Expected_Demand]
      ,[Target_Inventory]
	  ,[Z-value_ADJ] = [Z-value]
	  ,[Target_Inventory_ADJ] = CONVERT(DECIMAL(10,3), [Z-value] * SQRT(Mu_L * POWER(Sigma_D,2) + POWER(Mu_D,2) * POWER(Sigma_L,2))) + [Expected_Demand]
      ,[RN] = Row_Number() Over (Order by [SafetyStock].WSLR, GPDCN, Date)
  INTO [AB].[dbo].[Gamma_In]
  FROM [AB].[dbo].[SafetyStock]

LEFT JOIN 
       inpu ON
	   inpu.WSLR = [AB].[dbo].[SafetyStock].WSLR

 WHERE [Status] <> 'Unknown'

--CSV For R
SELECT [RN]
      ,[Sigma]
      ,[Mu] 
  FROM [AB].[dbo].[Gamma_In] 
 Where [Sigma] > 0
 Order By [RN]
--Save results as a CSV file
;
--Import "Gamma_R_Output" from R

WITH input5 AS (
SELECT [RN]
      ,[Alpha]
      ,[Beta]
      ,[Target_Inv.(Gamma)] = CONVERT(float,[Target_Inv.(Gamma)])
  FROM [AB].[dbo].[Gamma_R_Output])

SELECT [Date]
      ,[Wslr]
      ,[Gpdcn]
      ,[Brwy]
      ,[AC_Final]
      ,[Wsc_Dc]
      ,[SalesQty_BBL] = CEILING([Vol])
      ,[CASES_BBL]
      ,[T1] = CONVERT(DECIMAL(10,3),[T1])
      ,[T1_sd] = CONVERT(DECIMAL(10,3),[T1_sd])
      ,[T2] = CONVERT(DECIMAL(10,3),[T2])
      ,[T2_sd] = CONVERT(DECIMAL(10,3),[T2_sd])
      ,[T3] = CONVERT(DECIMAL(10,3),[T3])
      ,[T3_sd] = CONVERT(DECIMAL(10,3),[T3_sd])
      ,[Cycle]
      ,[ActualDemandDLT_BBL] = CEILING([Actual_Demand]/[CASES_BBL])
      ,[Target_Inv.(Normal)_BBL] = CEILING([Target_Inventory]/[CASES_BBL])
      ,[Status_Norm] =  IIF (Actual_Demand IS NULL, 'Unknown', 
                        IIF (CEILING([Target_Inventory]/[CASES_BBL]) >= CEILING([Actual_Demand]/[CASES_BBL]), 'OverStock', 'Shortfall'))
      ,[Inv.N-AD] = ABS(CEILING([Target_Inventory]/[CASES_BBL]) - CEILING([Actual_Demand]/[CASES_BBL]))
      ,[Z-value_ADJ]
      ,[Target_Inv.(ADJ)_BBL] = CEILING([Target_Inventory_ADJ]/[CASES_BBL])
      ,[Status_ADJ] =  IIF ([Target_Inventory_ADJ] IS NULL, 'Unknown', 
                       IIF (CEILING([Target_Inventory_ADJ]/[CASES_BBL]) >= CEILING([Actual_Demand]/[CASES_BBL]), 'OverStock', 'Shortfall'))
      ,[Inv.ADJ-AD] = ABS(CEILING([Target_Inventory_ADJ]/[CASES_BBL]) - CEILING([Actual_Demand]/[CASES_BBL]))
      ,[Target_Inv.(Gamma)_BBL] = IIF([Target_Inv.(Gamma)] IS Null, CEILING([Target_Inventory]/[CASES_BBL]), 
                                  IIF([Target_Inv.(Gamma)] = 0, CEILING([Target_Inventory]/[CASES_BBL]), CEILING([Target_Inv.(Gamma)]/[CASES_BBL]))) 
      ,[Status_Gama] =  IIF (Actual_Demand IS NULL, 'Unknown', 
                        IIF (CEILING([Target_Inv.(Gamma)]/[CASES_BBL]) >= CEILING([Actual_Demand]/[CASES_BBL]), 'OverStock', 'Shortfall'))
      ,[Inv.G-AD] = ABS(CEILING([Target_Inv.(Gamma)]/[CASES_BBL]) - CEILING([Actual_Demand]/[CASES_BBL]))
  INTO [AB].[dbo].[SafetyStockNormGama]
  FROM [AB].[dbo].[Gamma_In]
  Left Join 
       input5 ON
       input5.[RN] = [AB].[dbo].[Gamma_In].[RN]
