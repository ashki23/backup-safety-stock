With AC15_16 AS
(Select * , [Year] = 2015
From [AB].[dbo].[AC_2015]
Union ALL 
Select * , [Year] = 2016
From [AB].[dbo].[AC_2016])

SELECT IIF(LEN(Wslr)=5,Wslr,IIF(LEN(Wslr)=4,STUFF(Wslr,1,0,'0'),IIF(LEN(Wslr)=3,STUFF(Wslr,1,0,'00'),NULL))) AS WSLR
      ,IIF(LEN(Pdcn)=5,Pdcn,IIF(LEN(Pdcn)=4,STUFF(Pdcn,1,0,'0'),IIF(LEN(Pdcn)=3,STUFF(Pdcn,1,0,'00'),NULL))) AS GPDCN
      ,[AC_Final]
      ,[Year]
  INTO [AB].[dbo].[AC_15_16_Update]
  FROM AC15_16
