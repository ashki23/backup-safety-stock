WITH input3 AS (
SELECT *
      ,[Total_Vol_WSLR] = Sum([Vol]) Over (Partition By [WSLR] Order by WSLR) 
      ,[RN] = Row_Number() Over (Partition by [WSLR], [Status] Order by WSLR)
      ,[#Records] = Count(*) Over (Partition by [WSLR] Order by WSLR)
      ,[#Shortfall/Excess_WSLR] = Count(*) Over (Partition by [WSLR], [Status] Order by WSLR)
      ,[%Shortfall/Excess_WSLR] = (Count(*) Over (Partition by [WSLR], [Status] Order by WSLR) * 1.0) / (Count(*) Over (Partition by [WSLR] Order by WSLR) * 1.0)
  FROM [AB].[dbo].[SafetyStock]
 Where [Status] <> 'Unknown')

SELECT *    
      ,[Excess/Shortfall] = [Excess/Shortfall_Amount]
      ,[Excess/Shortfall_BBL] = [Excess/Shortfall_Vol]
      ,[#Shortfall] = [#Shortfall/Excess_WSLR]
      ,[Shortfall%] = [%Shortfall/Excess_WSLR] 
  INTO [AB].[dbo].[AlterZ_Shortfall_WSLR]
  FROM input3
 Where [%Shortfall/Excess_WSLR] > 0.0022 and
       [Status] = 'Shortfall'

GO
While (Select Max([Shortfall%]) 
       From [AB].[dbo].[AlterZ_Shortfall_WSLR]) > 0.0022
Begin
     Update [AB].[dbo].[AlterZ_Shortfall_WSLR] SET
            [Z] = CASE
                  When [Shortfall%] > 0.0022 Then Z + 0.5
		  Else Z End			
     Update [AB].[dbo].[AlterZ_Shortfall_WSLR] SET
            [SS] = [Z] * SQRT(Mu_L * POWER(Sigma_D,2) + POWER(Mu_D,2) * POWER(Sigma_L,2))
     Update [AB].[dbo].[AlterZ_Shortfall_WSLR] SET
            [Target_Inventory] = SS + Expected_Demand 
     Update [AB].[dbo].[AlterZ_Shortfall_WSLR] SET
            [Status] = CASE  
                       WHEN CEILING(Target_Inventory) >= CEILING(Actual_Demand) Then 'OverStock'
                       ELSE 'Shortfall' END
     Update [AB].[dbo].[AlterZ_Shortfall_WSLR] SET
            [Excess/Shortfall] = ABS(CEILING(Target_Inventory) - CEILING(Actual_Demand))
     Update [AB].[dbo].[AlterZ_Shortfall_WSLR] SET
            [Excess/Shortfall_BBL] = [Excess/Shortfall]/CASES_BBL; 

	 With Shortfall_Update AS (
	 Select [Wslr] , Status,
	        Count(CASE When [Status] = 'Shortfall' Then 1 Else 0 End) AS [Shortfall_num]
			From [AB].[dbo].[AlterZ_Shortfall_WSLR]
			Where Status= 'Shortfall'
			Group by [Wslr], Status)		

	 Update [AB].[dbo].[AlterZ_Shortfall_WSLR] SET
            [#Shortfall] = IIF([Shortfall_num] IS NULL, 0, [Shortfall_num])
            From [AB].[dbo].[AlterZ_Shortfall_WSLR]
  Left Join Shortfall_Update ON 
			Shortfall_Update.Wslr = [AB].[dbo].[AlterZ_Shortfall_WSLR].WSLR

     Update [AB].[dbo].[AlterZ_Shortfall_WSLR] SET
            [Shortfall%] = [#Shortfall]*1.0/[#Records]*1.0

 IF (Select Max([Shortfall%]) 
       From [AB].[dbo].[AlterZ_Shortfall_WSLR]) <= 0.0022
      Break
    Else
  Continue 
End 

Go
SELECT [WSLR]
      ,CONVERT(float, CEILING ([Total_Vol_WSLR])) AS [Total_BBL_WSLR]
      ,[AvgShort_ADJ] = CEILING (AVG([Excess/Shortfall_BBL]) Over (Partition By WSLR, Status Order By WSLR))
      ,[#Records]
      ,[#Shortfall/Excess_WSLR] AS [#Shortfall_Z=2.85]
      ,FORMAT([%Shortfall/Excess_WSLR], 'P') AS [%Shortfall_Z=2.85]
      ,CONVERT(DECIMAL(8,2),Z) AS [Z-value]
      ,[#Shortfall] AS [#Shortfall_Z-value]
      ,FORMAT([Shortfall%], 'P') AS [%Shortfall_Z-value]
  INTO [AB].[dbo].[AlterZ_Shortfall_WSLR_Results]
  FROM [AB].[dbo].[AlterZ_Shortfall_WSLR]
 Where [RN] = 1
 Order by [WSLR]